# Databricks notebook source
import csv
import time

data = []
walled = []
hash_table = {}

# Read data from filestore
rdd = sc.textFile("/FileStore/tables/5cxu79mj1487211025998")
tsv = sc.textFile("/FileStore/tables/4eu3vpga1487211676891")

# Read walled data
for line in rdd.collect():
    walled.append(line.strip().encode('utf8'))

# Get avaiables from each line of the data.
for i in tsv.collect():
  
  seconds, cid, value = i.encode('utf8').split()
  # Parse seconds and calculate which bucket should be placed.
  bucket = int(time.ctime(int(seconds)).split()[3].split(':')[1]) // 5
  values = value.split(',')
  # remove elements in wall of.txt
  for i in walled:
    while i in values:
      values.remove(i)
  # put results into hashtable
  if cid not in hash_table:
    hash_table[cid] = [[] for _ in range(12)]
  for i in values:
    hash_table[cid][bucket].append(i)
#write results to a file with the specific format
with open('../Output.txt', 'w') as output:
    output.write('|user_id|0-4|5-9|10-14|15-19|20-24|25-29|30-34|35-39|40-44|45-49|50-54|55-59|' + '\n')
    for key in hash_table:
      cur = '|' + key + '|'
      for i in hash_table[key]:
        cur = cur + ','.join(i) + '|'
      output.write(cur + '\n')
